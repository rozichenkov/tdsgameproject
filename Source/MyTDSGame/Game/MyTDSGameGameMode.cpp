// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Game/MyTDSGameGameMode.h"
#include "Game/MyTDSGamePlayerController.h"
#include "Character/MyTDSGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyTDSGameGameMode::AMyTDSGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMyTDSGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
