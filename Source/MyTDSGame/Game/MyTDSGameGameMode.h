

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyTDSGameGameMode.generated.h"

UCLASS(minimalapi)
class AMyTDSGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyTDSGameGameMode();
};



