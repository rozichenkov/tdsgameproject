// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Game/MyTDSGamePlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Character/MyTDSGameCharacter.h"
#include "Engine/World.h"

AMyTDSGamePlayerController::AMyTDSGamePlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void AMyTDSGamePlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void AMyTDSGamePlayerController::SetupInputComponent()
{
	
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AMyTDSGamePlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, & AMyTDSGamePlayerController::OnSetDestinationReleased);
 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMyTDSGamePlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AMyTDSGamePlayerController::MoveToTouchLocation);

	InputComponent->BindAction("ResetVR", IE_Pressed, this, &AMyTDSGamePlayerController::OnResetVR);
}

void AMyTDSGamePlayerController::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AMyTDSGamePlayerController::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (AMyTDSGameCharacter* MyPawn = Cast<AMyTDSGameCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			
			SetNewMoveDestination(Hit.ImpactPoint);
		}
	}
}

void AMyTDSGamePlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void AMyTDSGamePlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		
		if ((Distance > 120.0f))
		{
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void AMyTDSGamePlayerController::OnSetDestinationPressed()
{
	
	bMoveToMouseCursor = true;
}

void AMyTDSGamePlayerController::OnSetDestinationReleased()
{
	
	bMoveToMouseCursor = false;
}


